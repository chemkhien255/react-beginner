import { useEffect, useState } from "react";
import { FaPlusCircle } from "react-icons/fa";
import { getAllUsers, getUserWithPaginate } from "../../../services/apiService";
import "./ManageUser.scss";
import ModelCreateUser from "./ModelCreateUser";
import ModelDeleteUser from "./ModelDeleteUser";
import ModelFetchUser from "./ModelFetchUser";
import ModelUpdateUser from "./ModelUpdateUser";
import TableUserPaginate from "./TableUserPaginate";

const ManageUser = (props) => {
  const LIMIT_USER = 3;
  const [currentPage, setCurrentPage] = useState(1);
  const [pageCount, setPageCount] = useState(0);
  // Create user
  const [showModelCreateUser, setShowModelCreateUser] = useState(false);
  const [listUsers, setListUsers] = useState([]);
  useEffect(() => {
    fetchListUsersWithPaginate(1);
  }, []);
  const fetchListUsers = async () => {
    let res = await getAllUsers();
    if (res?.EC === 0) {
      setListUsers(res.DT);
    }
  };

  // fetch user with paginate
  const fetchListUsersWithPaginate = async (page) => {
    let res = await getUserWithPaginate(page, LIMIT_USER);
    if (res.EC === 0) {
      setListUsers(res.DT.users);
      setPageCount(res.DT.totalPages);
    }
  };

  //Update user
  const [showModelUpdateUser, setShowModelUpdateUser] = useState(false);
  const [dataUpdateUser, setDataUpdateUser] = useState([]);
  const handleClickBtnUpdate = (user) => {
    setShowModelUpdateUser(true);
    setDataUpdateUser(user);
  };
  const resetUpdateData = () => {
    setDataUpdateUser({});
  };

  // fetch user by id
  const [showModelFetchUser, setShowModelFetchUser] = useState(false);
  const [dataFetchUser, setDataFetchUser] = useState([]);
  const handleClickBtnFetch = (user) => {
    setShowModelFetchUser(true);
    setDataFetchUser(user);
  };
  const resetFetchData = () => {
    setDataFetchUser({});
  };

  // delete user
  const [showModelDeleteUser, setShowModelDeleteUser] = useState(false);
  const [dataDelete, setDataDelete] = useState([]);
  const handleClickBtnDelete = (user) => {
    setShowModelDeleteUser(true);
    setDataDelete(user);
  };

  return (
    <div className="manage-user-container">
      <div className="title">Manage User</div>
      <div className="user-content">
        <div className="btn-add-new">
          <button
            className="btn btn-primary"
            onClick={() => setShowModelCreateUser(true)}
          >
            <FaPlusCircle />
            Add new users
          </button>
        </div>
        <div className="table-users-container">
          {/* <TableUser
            listUsers={listUsers}
            handleClickBtnUpdate={handleClickBtnUpdate}
            handleClickBtnFetch={handleClickBtnFetch}
            handleClickBtnDelete={handleClickBtnDelete}
          /> */}
          <TableUserPaginate
            listUsers={listUsers}
            handleClickBtnUpdate={handleClickBtnUpdate}
            handleClickBtnFetch={handleClickBtnFetch}
            handleClickBtnDelete={handleClickBtnDelete}
            fetchListUsersWithPaginate={fetchListUsersWithPaginate}
            pageCount={pageCount}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
        </div>
        <ModelCreateUser
          show={showModelCreateUser}
          setShow={setShowModelCreateUser}
          fetchListUsers={fetchListUsers}
          fetchListUsersWithPaginate={fetchListUsersWithPaginate}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
        />
        <ModelUpdateUser
          show={showModelUpdateUser}
          setShow={setShowModelUpdateUser}
          dataUpdateUser={dataUpdateUser}
          fetchListUsers={fetchListUsers}
          resetUpdateData={resetUpdateData}
          fetchListUsersWithPaginate={fetchListUsersWithPaginate}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
        />
        <ModelFetchUser
          show={showModelFetchUser}
          setShow={setShowModelFetchUser}
          dataFetchUser={dataFetchUser}
          fetchListUsers={fetchListUsers}
          resetFetchData={resetFetchData}
        />
        <ModelDeleteUser
          show={showModelDeleteUser}
          setShow={setShowModelDeleteUser}
          dataDelete={dataDelete}
          fetchListUsers={fetchListUsers}
          fetchListUsersWithPaginate={fetchListUsersWithPaginate}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
        />
      </div>
    </div>
  );
};
export default ManageUser;
