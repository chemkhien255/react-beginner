import _ from "lodash";
import { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import { toast } from "react-toastify";
import { putUpdateQuiz } from "../../../../services/apiService";

const ModelUpdateQuiz = (props) => {
  const options = [
    { value: "EASY", label: "EASY" },
    { value: "MEDIUM", label: "MEDIUM" },
    { value: "HARD", label: "HARD" },
  ];
  const { show, setShow, dataUpdateQuiz, setDataUpdateQuiz } = props;
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [type, setType] = useState("");
  const [image, setImage] = useState(null);
  const [previewImage, setPreviewImage] = useState("");
  const handleClose = () => {
    setShow(false);
    setName("");
    setDescription("");
    setType("");
    setImage("");
    setPreviewImage("");
    setDataUpdateQuiz({});
  };

  useEffect(() => {
    if (!_.isEmpty(dataUpdateQuiz)) {
      //update state
      setDescription(dataUpdateQuiz.description);
      setName(dataUpdateQuiz.name);
      setType(dataUpdateQuiz.difficulty);
      setImage("");
      if (dataUpdateQuiz.image) {
        setPreviewImage(`data:image/jpeg;base64,${dataUpdateQuiz.image}`);
      }
    }
  }, [props.dataUpdateQuiz]);

  const handleSubmitUpdateQuiz = async () => {
    if (!name) {
      toast.error("Invalid name");
      return;
    }

    if (!description) {
      toast.error("Invalid description");
      return;
    }
    let data = await putUpdateQuiz(
      dataUpdateQuiz.id,
      name,
      description,
      type,
      image
    );
    if (data?.EC === 0) {
      toast.success(data.EM);
      await props.fetchQuiz();
      handleClose();
    }
    if (data?.EC !== 0) {
      toast.error(data.EM);
    }
  };

  const handleUploadImage = (event) => {
    if (event.target && event.target.files && event.target.files[0]) {
      setPreviewImage(URL.createObjectURL(event.target.files[0]));
      setImage(event.target.files[0]);
    } else {
      // setPreviewImage("");
    }
  };
  return (
    <>
      {/* <Button variant="primary" type="submit" onClick={handleShow}>
      Launch demo modal
    </Button> */}

      <Modal
        show={show}
        onHide={handleClose}
        size="lg"
        backdrop="static"
        className="modal-add-quiz"
      >
        <Modal.Header closeButton>
          <Modal.Title>Update a quiz</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Row className="mb-12">
              <Form.Group as={Col}>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>
            </Row>
            <Row className="mb-12">
              <Form.Group as={Col}>
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>
            </Row>
            <Row className="mb-12">
              <Form.Group as={Col}>
                <Form.Label>Upload File Image</Form.Label>
                <Form.Control
                  type="file"
                  id="labelUpload"
                  onChange={(e) => handleUploadImage(e)}
                />
              </Form.Group>
            </Row>
            <Row className="mb-12">
              {previewImage ? (
                <img src={previewImage} />
              ) : (
                <span>Preview Image</span>
              )}
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handleSubmitUpdateQuiz()}>
            Save
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default ModelUpdateQuiz;
