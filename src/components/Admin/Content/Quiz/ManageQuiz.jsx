import { useState } from "react";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Select from "react-select";
import { toast } from "react-toastify";
import { postCreateNewQuiz } from "../../../../services/apiService";
import AssignQuiz from "./AssignQuiz";
import "./ManageQuiz.scss";
import QuizQA from "./QuizQA";
import TableQuiz from "./TableQuiz";

const ManageQuiz = (props) => {
  const options = [
    { value: "EASY", label: "EASY" },
    { value: "MEDIUM", label: "MEDIUM" },
    { value: "HARD", label: "HARD" },
  ];
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [type, setType] = useState("");
  const [image, setImage] = useState(null);

  const handleChangeFile = (e) => {
    if (e?.target?.files?.[0]) {
      setImage(e.target.files[0]);
    }
  };

  const handleSubmitQuiz = async () => {
    // validate

    if (!name || !description) {
      toast.error("Name/Description is required!");
      return;
    }
    //
    let res = await postCreateNewQuiz(description, name, type?.value, image);
    if (res && res.EC === 0) {
      toast.success(res.EM);
      setName("");
      setDescription("");
      setImage(null);
    } else {
      toast.error(res.EM);
    }
  };

  return (
    <div className="quiz-container">
      <Accordion defaultActiveKey="0">
        <Accordion.Item eventKey="0">
          <Accordion.Header>Manage Quizzes</Accordion.Header>
          <Accordion.Body>
            <div className="add-new">
              <fieldset className="border rounded-3 p-3">
                <legend className="float-none w-auto px-3">
                  Add new Quiz:
                </legend>
                <Form>
                  <Form.Group className="mb-3">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="your quiz name"
                      value={name}
                      onChange={(event) => setName(event.target.value)}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="your description"
                      value={description}
                      onChange={(event) => setDescription(event.target.value)}
                    />
                  </Form.Group>
                  <div className="my-3">
                    <label> Select the level</label>
                    <Select
                      options={options}
                      defaultValue={type}
                      onChange={setType}
                      placeholder={"Quiz type..."}
                    />
                  </div>
                  <label>Upload Image</label>
                  <div className="more-actions">
                    <input
                      type="file"
                      className="form-control"
                      onChange={(e) => handleChangeFile(e)}
                    />
                  </div>
                </Form>
                <div className="mt-3">
                  <Button
                    type="submit"
                    className="my-1 btn btn-warning"
                    onClick={() => {
                      handleSubmitQuiz();
                    }}
                  >
                    Submit
                  </Button>
                </div>
              </fieldset>
            </div>

            <div className="list-detail">
              <TableQuiz />
            </div>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="1">
          <Accordion.Header>Update Question - Answer</Accordion.Header>
          <Accordion.Body>
            <QuizQA />
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="2">
          <Accordion.Header>Assign to Users</Accordion.Header>
          <Accordion.Body>
            <AssignQuiz />
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </div>
  );
};
export default ManageQuiz;
