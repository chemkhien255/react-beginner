import { useEffect, useState } from "react";
import Select from "react-select";
import {
  getAllQuizForAdmin,
  getAllUsers,
  postAssignQuiz,
} from "../../../../services/apiService";
import { toast } from "react-toastify";

const AssignQuiz = (props) => {
  const [selectedQuiz, setSelectedQuiz] = useState([]);
  const [listQuiz, setListQuiz] = useState([]);
  const [listUser, setListUser] = useState([]);
  const [selectedUser, setSelectedUser] = useState([]);
  const selectStyles = {
    menu: (base) => ({
      ...base,
      zIndex: 100,
    }),
  };
  useEffect(() => {
    fetchQuiz();
    fetchUser();
  }, []);

  const fetchQuiz = async () => {
    let res = await getAllQuizForAdmin();
    if (res && res.EC === 0) {
      let newQuiz = res.DT.map((item) => {
        return {
          value: item.id,
          label: `${item.id} - ${item.description}`,
        };
      });
      setListQuiz(newQuiz);
    }
  };
  const fetchUser = async () => {
    let res = await getAllUsers();
    if (res && res.EC === 0) {
      let users = res.DT.map((item) => {
        return {
          value: item.id,
          label: `${item.id} - ${item.username} - ${item.email}`,
        };
      });
      setListUser(users);
    }
  };

  const handleAssign = async () => {
    let res = await postAssignQuiz(selectedQuiz.value, selectedUser.value);
    if (res && res.EC === 0) {
      toast.success(res.EM);
      setSelectedQuiz([]);
      setSelectedUser([]);
    } else {
      toast.error(res.EM);
    }
  };
  return (
    <div className="assign-quiz-container row">
      <div className="col-6 from-group">
        <label className="mb-2">Select Quiz:</label>
        <Select
          styles={selectStyles}
          options={listQuiz}
          defaultValue={selectedQuiz}
          onChange={setSelectedQuiz}
        />
      </div>
      <div className="col-6 from-group">
        <label className="mb-2">Select Users:</label>
        <Select
          styles={selectStyles}
          options={listUser}
          defaultValue={selectedUser}
          onChange={setSelectedUser}
        />
      </div>
      <div>
        <button
          className="btn btn-warning mt-3"
          onClick={() => {
            handleAssign();
          }}
        >
          Assign
        </button>
      </div>
    </div>
  );
};
export default AssignQuiz;
