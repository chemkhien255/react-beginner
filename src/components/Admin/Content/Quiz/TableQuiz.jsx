import { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import { getAllQuizForAdmin } from "../../../../services/apiService";
import ModelDeleteQuiz from "./ModelDeleteQuiz";
import ModelUpdateQuiz from "./ModelUpdateQuiz";

const TableQuiz = (props) => {
  const [listQuiz, setListQuiz] = useState([]);

  useEffect(() => {
    fetchQuiz();
  }, []);

  const fetchQuiz = async () => {
    setDataDeleteQuiz({});
    setDataUpdateQuiz({});
    let res = await getAllQuizForAdmin();
    if (res && res.EC === 0) {
      setListQuiz(res.DT);
    }
  };

  //Update quiz
  const [showModelUpdateQuiz, setShowModelUpdateQuiz] = useState(false);
  const [dataUpdateQuiz, setDataUpdateQuiz] = useState([]);
  const handleClickBtnUpdate = (quiz) => {
    setShowModelUpdateQuiz(true);
    setDataUpdateQuiz(quiz);
  };

  //Delete quiz
  const [showModelDeleteQuiz, setShowModelDeleteQuiz] = useState(false);
  const [dataDeleteQuiz, setDataDeleteQuiz] = useState([]);
  const handleClickBtnDelete = (quiz) => {
    setShowModelDeleteQuiz(true);
    setDataDeleteQuiz(quiz);
  };

  return (
    <>
      <div>List Quizzes: </div>
      <Table striped bordered hover className="my-2">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {listQuiz &&
            listQuiz.map((item, index) => {
              return (
                <tr key={`table-quiz-${index}`}>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.description}</td>
                  <td>{item.difficulty}</td>
                  <td className="d-flex gap-2">
                    <button
                      className="btn btn-warning"
                      onClick={() => {
                        handleClickBtnUpdate(item);
                      }}
                    >
                      Edit
                    </button>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        handleClickBtnDelete(item);
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </Table>
      <ModelUpdateQuiz
        show={showModelUpdateQuiz}
        setShow={setShowModelUpdateQuiz}
        dataUpdateQuiz={dataUpdateQuiz}
        fetchQuiz={fetchQuiz}
        setDataUpdateQuiz={setDataUpdateQuiz}
      />
      <ModelDeleteQuiz
        show={showModelDeleteQuiz}
        setShow={setShowModelDeleteQuiz}
        dataDeleteQuiz={dataDeleteQuiz}
        fetchQuiz={fetchQuiz}
      />
    </>
  );
};

export default TableQuiz;
