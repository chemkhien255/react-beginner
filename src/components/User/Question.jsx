import _ from "lodash";
import Form from "react-bootstrap/Form";
import Lightbox from "react-awesome-lightbox";
import { useState } from "react";
const Question = (props) => {
  const { data, index } = props;

  const handleCheckbox = (e, answerId, questionId) => {
    props.handleCheckbox(answerId, questionId);
  };

  const [isPreviewImage, setIsPreviewImage] = useState(false);

  if (_.isEmpty(data)) {
    return <></>;
  }
  return (
    <>
      {data.image ? (
        <div className="q-image">
          <img
            onClick={() => {
              isPreviewImage(true);
            }}
            src={`data:image/jpeg;base64,${data.image}`}
            style={{ cursor: "pointer" }}
          />
          {isPreviewImage === true && (
            <Lightbox
              onClose={() => setIsPreviewImage(false)}
              image={`data:image/jpeg;base64,${data.image}`}
              title={`Question image`}
            />
          )}
        </div>
      ) : (
        <div className="q-image"></div>
      )}
      <div className="question">
        Question {index + 1}: {data.questionDescription}
      </div>
      <div className="answer">
        {data.answers &&
          data.answers.length &&
          data.answers.map((a, index) => {
            return (
              <div key={`answer-${index}`} className="a-child">
                <Form>
                  <div className="mb-3">
                    <Form.Check
                      type={"checkbox"}
                      label={a.description}
                      checked={a.isSelected}
                      onChange={(e) => handleCheckbox(e, a.id, data.questionId)}
                    />
                  </div>
                </Form>
              </div>
            );
          })}
      </div>
    </>
  );
};

export default Question;
