import { useState } from "react";
import { FaSpinner } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { doLogin } from "../../redux/action/userAction";
import { postLogin } from "../../services/apiService";
import "./SignIn.scss";
import Language from "../Header/Language";

const SignIn = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const validateEmail = (email) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };
  const handleLogin = async () => {
    // validate
    const isValidEmail = validateEmail(email);
    if (!isValidEmail) {
      toast.error("Invalid email");
      return;
    }

    if (!password) {
      toast.error("invalid password");
      return;
    }

    setIsLoading(true);

    // submit api
    let data = await postLogin(email, password);
    if (data?.EC === 0) {
      dispatch(doLogin(data));
      toast.success(data.EM);
      setIsLoading(false);
      navigate("/");
    }
    if (+data?.EC !== 0) {
      toast.error(data.EM);
      setIsLoading(false);
    }
  };
  const handleNavigate = () => {
    navigate("/register");
  };

  const handleKeyDown = (e) => {
    if (e && e.key === `Enter`) {
      handleLogin();
    }
  };

  return (
    <div className="login-container">
      <div className="header mx-auto">
        <span>Chưa có tài khoản?</span>
        <button onClick={handleNavigate}>Sign up</button>
        <Language />
      </div>
      <div className="title col-4 mx-auto">Actilazion</div>
      <div className="welcome col-4 mx-auto">Xin chào, ai vậy đó?</div>
      <div className="content-form col-4 mx-auto">
        <div className="form-group">
          <label htmlFor="">Email</label>
          <input
            type="text"
            className="form-control"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">Password</label>
          <input
            type="password"
            className="form-control"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            onKeyDown={(e) => handleKeyDown(e)}
          />
        </div>
        <span className="forgot-password">Quên mật khẩu?</span>
        <div>
          {" "}
          <button
            className="btn-submit"
            onClick={() => handleLogin()}
            disabled={isLoading}
          >
            {isLoading === true && <FaSpinner className="loader-icon" />}
            <span>Login to Actilazion</span>
          </button>
        </div>
        <div className="text-center">
          <span
            className="back"
            onClick={() => {
              navigate("/");
            }}
          >
            {" "}
            &#60; &#60;Go to Homepage
          </span>
        </div>
      </div>
    </div>
  );
};
export default SignIn;
