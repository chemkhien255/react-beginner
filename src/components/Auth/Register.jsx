import { useState } from "react";
import { FaRegEye, FaRegEyeSlash } from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { postRegister } from "../../services/apiService";

import "./Register.scss";
import Language from "../Header/Language";

const Register = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const [type, setType] = useState("password");
  const [icon, setIcon] = useState(true);
  const navigate = useNavigate();

  const handleToggle = () => {
    if (type === "password") {
      setIcon(false);
      setType("text");
    } else {
      setIcon(true);
      setType("password");
    }
  };

  const validateEmail = (email) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };

  const handleRegister = async () => {
    // validate
    const isValidEmail = validateEmail(email);
    if (!isValidEmail) {
      toast.error("Invalid email");
      return;
    }

    if (!password) {
      toast.error("invalid password");
      return;
    }
    // submit api
    let data = await postRegister(email, password, username);
    if (+data?.EC === 0) {
      toast.success(data.EM);
      navigate("/login");
    }
    if (+data?.EC !== 0) {
      toast.error(data.EM);
    }
  };

  const handleNavigate = () => {
    navigate("/login");
  };
  return (
    <div className="register-container">
      <div className="header mx-auto">
        <span>Đã có tài khoản?</span>
        <button onClick={() => handleNavigate()}>Login</button>
        <Language />
      </div>
      <div className="title col-4 mx-auto">Actilazion</div>
      <div className="welcome col-4 mx-auto">Sẵn sàng cho các câu hỏi mới?</div>
      <div className="content-form col-4 mx-auto">
        <div className="form-group">
          <label htmlFor="">Email</label>
          <input
            type="text"
            className="form-control"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">Password</label>
          <input
            type={type}
            className="form-control"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            autoComplete="current-password"
          />
          <span onClick={handleToggle} className="password-icon">
            {icon ? <FaRegEyeSlash /> : <FaRegEye />}
          </span>
        </div>
        <div className="form-group">
          <label htmlFor="">Username</label>
          <input
            type="text"
            className="form-control"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div>
          <button className="btn-submit" onClick={() => handleRegister()}>
            Tạo một tài khoản mới
          </button>
        </div>
        <div className="text-center">
          <span
            className="back"
            onClick={() => {
              navigate("/");
            }}
          >
            {" "}
            &#60; &#60;Go to Homepage
          </span>
        </div>
      </div>
    </div>
  );
};

export default Register;
