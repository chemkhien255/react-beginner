import { Route, Routes } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import App from "./App.jsx";
import Admin from "./components/Admin/Admin.jsx";
import Dashboard from "./components/Admin/Content/Dashboard.jsx";
import ManageUser from "./components/Admin/Content/ManageUser.jsx";
import Questions from "./components/Admin/Content/Question/Questions.jsx";
import ManageQuiz from "./components/Admin/Content/Quiz/ManageQuiz.jsx";
import Register from "./components/Auth/Register.jsx";
import SignIn from "./components/Auth/SignIn.jsx";
import HomePage from "./components/Home/Homepage.jsx";
import DetailQuiz from "./components/User/DetailQuiz.jsx";
import ListQuiz from "./components/User/ListQuiz.jsx";
import PrivateRoute from "./routes/PrivateRoute.jsx";
import { Suspense } from "react";

const NotFound = () => {
  return (
    <div className="container alert mt-3 alert-danger text-center">
      Not found data with your current URL!
    </div>
  );
};
const Layout = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<HomePage />} />
          <Route
            path="users"
            element={
              <PrivateRoute>
                <ListQuiz />
              </PrivateRoute>
            }
          />
        </Route>
        <Route path="/quiz/:id" element={<DetailQuiz />} />
        <Route
          path="admins"
          element={
            <PrivateRoute>
              <Admin />
            </PrivateRoute>
          }
        >
          <Route index element={<Dashboard />} />
          <Route path="manage-users" element={<ManageUser />} />
          <Route path="manage-quizzes" element={<ManageQuiz />} />
          <Route path="manage-questions" element={<Questions />} />
        </Route>

        <Route path="login" element={<SignIn />} />
        <Route path="register" element={<Register />} />
        <Route path="/test" element={<PrivateRoute />}></Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
        transition:Bounce
      />
    </Suspense>
  );
};
export default Layout;
